\name{sciexdata}
\alias{sciexdata}

\title{AB Sciex LC-MS data files}

\description{
  The mzML files in the \code{sciex} directory in the \code{msdata}
  package represent profile-mode LC-MS data of pooled human serum
  samples (the same pool being measured). The samples were analyzed by
  ultra high-performance liquid chromatography (UHPLC; Agilent 1290)
  coupled to a Q-TOF mass spectrometer (TripleTOF 5600+ AB Sciex). The
  chromatographic separation was based in hydrophilic interaction liquid
  chromatography (HILIC) and performed using an Waters Acquity BEH
  Amide, 100 x 2.1 mm column.

  The mass specctrometer was operated in full scan mode in the mass
  range from 50 to 1000 m/z and with an accumulation time of 250 ms. The
  files represent a subset of spectra/scans from m/z 105 to 134 and from
  retention time 0 to 260 seconds. The files were generated in the same
  LC-MS run, but from different injections. Details on the individual
  files are provided below.
}

\details{
  \itemize{
    \item{20171016_POOL_POS_1_105-134.mzML}{
      profile-mode LC-MS data of pooled human serum samples. Injection
      index: 1.
    }

    \item{20171016_POOL_POS_3_105-134.mzML}{
      profile-mode LC-MS data of pooled human serum samples. Injection
      index: 19.
    }
  }
}

\author{
  Sigurdur Smarason, Giuseppe Paglia and Johannes Rainer
}

\examples{
## List the files in the sciex folder
dir(system.file("sciex", package = "msdata"))

}

\keyword{datasets}